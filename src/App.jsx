import React from "react";

import { BrowserRouter, Routes, Route } from "react-router-dom";
// import Layout from "./modules/layout/sitebar/Layout";
import Layout from "./modules/layout/Layout";
import Sitebar from "./modules/layout/sitebar/Sitebar";
import Home from "./modules/home/Home";
import Blog from "./modules/blog/Blog";
import Contact from "./modules/contact/Contact";
import Error from "./modules/error/Error";
import About from "./modules/about/About";
import "../src/assets/css/blog.css"

function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route index element={<Home />} />
            <Route path="/blog" element={<Blog />} />
            <Route path="/sitebar" element={<Sitebar />} />
            <Route path="/contact" element={<Contact/>} />
            <Route path="/about" element={<About/>}/>
          </Route>
          <Route path="*" element={<Error/>} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
