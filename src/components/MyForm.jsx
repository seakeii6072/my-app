import React, { useState } from "react";

class MyForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      form: {
        name: "",
        age: 0,
        gender: null,
        photo: null,
        programing: [],
        bDate: "",
      },
      records: [],
    };
  }

  setInputState = (key, event) =>
    this.setState((preState) => ({
      ...preState,
      form: {
        ...preState.form,
        [key]: event.target.value,
      },
    }));

  onChangeCheckBox = (event) => {
    let finalPro = [...this.state.form.programing];

    if (event.target.checked) {
      finalPro.push(event.target.value);
    } else {
      finalPro = finalPro.filter((e) => e != event.target.value);
    }

    this.setState((preState) => ({
      ...preState,
      form: {
        ...preState.form,
        programing: finalPro,
      },
    }));
  };

  onSubmit = () => {
    this.setState((preState) => ({
      records: [...preState.records, this.state.form],
    }));
  };

  render() {
    const { records } = this.state;

    return (
      <>
        <div className="container">
          <div className="p-5 row d-flex justify-content-center">
            <div className="col-md-6 border px-5 bg-secondary py-4">
              <h2 className="text-center pt-2 text-white fw-bold">
                Register Form
              </h2>
              <label className="w-100">
                <p className=" text-white">Enter Name:</p>
                <input

                  onChange={(event) => this.setInputState("name", event)}
                  className="form-control "
                  type="text"
                />
              </label>
              <br />
              <label className="w-100">
                <p className=" pt-2 text-white">Age</p>
                <input
                  onChange={(event) => this.setInputState("age", event)}
                  className="form-control "
                  type="number"
                />
              </label>
              <br />

              <label className="w-100">
                <p className=" pt-2 text-white">BirthDay</p>
                <input
                  onChange={(event) => this.setInputState("bDate", event)}
                  className="form-control "
                  type="date"
                />
              </label>
              <br />

              <label className="w-100">
                <p className=" pt-2 text-white">Gender</p>
                <select
                  onChange={(event) => this.setInputState("gender", event)}
                  className="form-control "
                >
                  <option value="male">Male</option>
                  <option value="female">Female</option>
                  <option value="other">Other</option>
                </select>
              </label>
              <br />

              <label className="w-100">
                <p className=" pt-2 text-white">Photo</p>
                <input
                  onChange={(event) => this.setInputState("photo", event)}
                  className="form-control "
                  type="file"
                />
              </label>
              <br />

              <label className="w-100">
                <p className=" pt-2 text-white">Phone</p>
                <input
                placeholder="Enter phone number"
                  onChange={(event) => this.setInputState("phone", event)}
                  className="form-control "
                  type="tel"
                />
              </label>
              <br />

              <label className="w-100">
                <p className="pt-2 text-white">Email</p>
                <input
                placeholder="Enter your email"
                  onChange={(event) => this.setInputState("email", event)}
                  className="form-control "
                  type="email"
                />
              </label>
              <br />

              <label className="p-2 w-100">
                <p className=" pt-2 text-white">Programing</p>
              </label>
              <br />
              <label
                onChange={this.onChangeCheckBox}
                className="mx-2  text-white"
              >
                <input value="Js" className="mx-1" type="checkbox" />
                Js
              </label>

              <label className="text-white">
                <input
                  onChange={this.onChangeCheckBox}
                  value="C++"
                  className="me-1"
                  type="checkbox"
                />
                C++
              </label>
              <br />

              <input
                onClick={this.onSubmit}
                className="form-control mt-3 w-25  bg-primary text-white m-auto"
                value="Submit"
                type="button"
              />
            </div>
            <div className="col-md-12 pt-5">
              <table className="table">
                <thead>
                  <tr className="table-dark text-white">
                    <th className="  text-white">Name</th>
                    <th className="  text-white">Age</th>
                    <th className="  text-white">Date of Birth</th>
                    <th className="  text-white">Gender</th>
                    <th className="  text-white">Phone</th>
                    <th className="  text-white">Photo</th>
                    <th className="  text-white">Email</th>
                    <th className="  text-white">Programing</th>
                  </tr>
                </thead>
                <tbody>
                  {records.length === 0 && (
                    <tr>
                      <td className="text-center" colSpan={6}>
                        No Record
                      </td>
                    </tr>
                  )}
                  {records.map((row, index) => {
                    return (
                      <tr key={index}>
                        <td>{row.name}</td>
                        <td>{row.age}</td>
                        <td>{row.bDate}</td>
                        <td>{row.gender}</td>
                        <td>{row.phone}</td>
                        <td>
                          <img src={row.photo} alt="__" />
                        </td>
                        <td>{row.email}</td>
                        <td>{row.programing}</td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default MyForm;
