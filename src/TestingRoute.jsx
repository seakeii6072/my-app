import React from 'react'
import {BrowserRouter, Routes, Route} from "react-router-dom";
import Sitebar from './modules/layout/sitebar/Sitebar';

const TestingRoute = () => {
  return (
    <div>
        <BrowserRouter>
            <Routes>
                <Route index path='/testing' element={<Sitebar />} />
            </Routes>
        </BrowserRouter>
        <h1>Testing Router in React.js</h1>
    </div>
  )
}

export default TestingRoute