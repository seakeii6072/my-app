import React from "react";
import imageFooter1 from "../../../assets/images/icon-pay-01.png";
import imageFooter2 from "../../../assets/images/icon-pay-02.png";
import imageFooter3 from "../../../assets/images/icon-pay-03.png";
import imageFooter4 from "../../../assets/images/icon-pay-04.png";
import imageFooter5 from "../../../assets/images/icon-pay-05.png";
import "../../../assets/css/footer.css";

const Footer = () => {
  return (
    <>
      <footer className="bg-black">
        <div className="container">
          <div className="row pt-5 justify-content-center">
            <div className="col-sm-6 col-lg-3 p-b-50">
              <div className="">
                <h4 className="fs-5 text-white">CATEGORIES</h4>
                <ul className="text-a pt-4">
                  <li className="p-2 text-footer-hover">
                    <a href="">Women</a>
                  </li>
                  <li className="p-2 text-footer-hover">
                    <a href="">Men</a>
                  </li>
                  <li className="p-2">
                    <a href="">Shoes</a>
                  </li>
                  <li className="p-2">
                    <a href="">Watches</a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-md-3">
              <div>
                <h4 className="fs-5 text-white">HELP</h4>
                <ul className="text-a pt-4">
                  <li className="p-2 text-footer-hover">
                    <a href="">Track Order</a>
                  </li>
                  <li className="p-2">
                    <a href="">Returns</a>
                  </li>
                  <li className="p-2">
                    <a href="">Shipping</a>
                  </li>
                  <li className="p-2">
                    <a href="">FAQs</a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-md-3">
              <div>
                <h4 className="fs-5 text-white">GET IN TOUCH</h4>
                <p className="text-white pt-4">
                  Any questions? Let us know in store at 8th floor, 379 Hudson
                  St, New York, NY 10018 or call us on (+1) 96 716 6879
                </p>
                <a className="icon-text " href="">
                  <i className="fa-brands fa-facebook-f p-2 fs-5 "></i>
                </a>{" "}
                <a className="icon-text" href="">
                  <i className="fa-brands fa-instagram p-2  fs-5 "></i>
                </a>{" "}
                <a className="icon-text" href="">
                  <i className="fa-solid fa-p p-2  fs-5 "></i>
                </a>
              </div>
            </div>

            <div className=" d-flex justify-content-center align-items-center p-5 hover-pointer">
              <img src={imageFooter1} alt="" />
              <img src={imageFooter2} alt="" />
              <img src={imageFooter3} alt="" />
              <img src={imageFooter4} alt="" />
              <img src={imageFooter5} alt="" />
            </div>
            <div className="">
              <p className="text-white text-center">
                Copyright ©2024 All rights reserved | Made with Beauty{" "}
                <a className="hover-footer" href="https://colorlib.com/">
                  Colorlib
                </a>{" "}
                & distributed by{" "}
                <a className="hover-footer" href="https://themewagon.com/">
                  ThemeWagon
                </a>
              </p>
            </div>
          </div>
        </div>
      </footer>
    </>
  );
};

export default Footer;
