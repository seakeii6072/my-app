import React from "react";
import { Link } from "react-router-dom";
import "../../../assets/css/header.css";

const Header = () => {
  return (
    <div className="position-sticky">
      <nav className="navbar navbar-expand-lg bg-black p-4 ">
        <div className="container">
          <Link className="navbar-brand me-5 " to="/">
            <span className="fw-bold">
              <i class="fa-brands fa-wordpress px-2"></i>Logo
            </span>
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item dropdown">
                <Link className="nav-link  text-black" to="/" role="button">
                  Home
                </Link>
              </li>

              <li className="nav-item">
                <Link
                  className="nav-link active"
                  aria-current="page"
                  to="/blog"
                >
                  Blog
                </Link>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link active"
                  aria-current="page"
                  href="/about"
                >
                  About
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link active"
                  aria-current="page"
                  href="/contact"
                >
                  Contact
                </a>
              </li>
            </ul>
            <div className="d-flex  ">
              <button type="button" className="btn position-relative">
                <i className=" icon1 fa-solid fa-magnifying-glass fs-4"></i>
              </button>
              <button type="button" className="btn position-relative">
                <i className=" icon2 fa-solid fa-cart-shopping fs-4"></i>
                <span className="position-absolute top-0 start-100 translate-middle badge bg-primary">
                  2
                </span>
              </button>
              <button type="button" className="btn position-relative">
                <i className=" icon3 fa-regular fa-heart fs-4"></i>
                <span className="position-absolute top-0 start-100 translate-middle badge bg-primary">
                  0
                </span>
              </button>
              <input class="form-control " type="search " placeholder="Search " />
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
};

export default Header;
