import React from "react";

const Contact = () => {
  return (
    <>
      <div className="banner_content d-flex ">
        <h1 className="text-center m-auto fw-bold">Contact</h1>
      </div>
      <div className="container">
        <div className="row text-center">
          <div className="col-md-6 border border-2 p-5 mt-5 card-left">
            <h3 className=" fs-3 my-3 pb-4">Send Us A Message</h3>
            <div className="mb-3 d-flex justify-content-center align-items-center border border-1 p-3">
              <i className="fa-solid fa-envelope fs-3"></i>
              <input
                type="email"
                className="form-control email-input border-0"
                id="exampleFormControlInput1"
                placeholder="name@example.com"
              />
            </div>
            <div className="mb-3">
              <textarea
                className="form-control message-input"
                id="exampleFormControlTextarea1"
                rows="3"
              ></textarea>
            </div>
            <a
              href="#"
              className="btn w-100 rounded-pill pt-2  text-white border border-1  mt-5 submit-hover"
            >
              SUBMIT
            </a>
          </div>
          <div className="col-md-6 text-start border  border-2 p-5 mt-5 card-right">
            <h4 className="pt-5">
              <i className="fa-solid fa-location-dot px-3"></i>Address
            </h4>
            <p className="pt-3 text-secondary text-Address">
              Coza Store Center 8th floor, 379
            </p>
            <p className="text-secondary text-Address">
              Hudson St, New York, NY 10018 US
            </p>
            <h4 className="pt-5 px-2">
              <i className="fa-solid fa-phone px-3"></i>Lets Talk
            </h4>
            <p className="pt-3  text-primary text-Address">+1 800 1236879</p>
            <h4 className="pt-5 px-2">
              <i className="fa-solid fa-envelope px-3"></i>Sale Support
            </h4>
            <p className=" pt-3 text-primary text-Address">
              contact@example.com
            </p>
          </div>
        </div>
      </div>
      <div class="google-map pt-5">
        <iframe width="100%" height="600"
            src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;coord=52.70967533219885, -8.020019531250002&amp;q=1%20Grafton%20Street%2C%20Dublin%2C%20Ireland&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed"
            frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><br />
      </div>
    </>
  );
};

export default Contact;
