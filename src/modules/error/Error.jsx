import React from "react";

const Error = () => {
  return (
    <>
      <div className="pt-5 text-danger">
        <h1 className="text-center  fw-bold">404</h1>
      </div>
    </>
  );
};

export default Error;
