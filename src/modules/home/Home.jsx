import React from "react";
import homeBanner from "../../assets/images/banner3.gif";

const Home = () => {
  return (
    <div>
      <div className="container">
        <div className="row">
          <div className="col-md-6">
            <h1 className="fw-bold pt-3">
              Web <br />
            </h1>
          </div>
          <div className="col-md-6">
            <img src={homeBanner} className="w-75 " />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
